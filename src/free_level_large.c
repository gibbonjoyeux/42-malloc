/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_level_large.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 14:33:54 by shuertas          #+#    #+#             */
/*   Updated: 2018/01/12 12:47:06 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

/*
********************************************************************************
** PUBLIC FUNCTIONS ************************************************************
********************************************************************************
*/

void				malloc_free_level_large(void)
{
	t_m_piece_l		*piece;
	t_m_piece_l		*piece_prev;
	t_m_piece_l		*piece_next;

	piece_prev = NULL;
	piece = g_malloc.large;
	while (piece)
	{
		if (piece->level == g_malloc.level)
		{
			piece_next = piece->next;
			if (piece_prev)
				piece_prev->next = piece->next;
			else
				g_malloc.large = piece_next;
			munmap((void*)piece, piece->length + sizeof(t_m_piece_l));
			piece = piece_next;
			continue;
		}
		piece_prev = piece;
		piece = piece->next;
	}
}
