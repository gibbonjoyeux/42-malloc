/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 14:33:54 by shuertas          #+#    #+#             */
/*   Updated: 2018/02/02 10:25:15 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

/*
********************************************************************************
** STATIC FUNCTIONS ************************************************************
********************************************************************************
*/

/*
********************************************************************************
** PUBLIC FUNCTIONS ************************************************************
********************************************************************************
*/

void			*calloc(size_t count, size_t size)
{
	void		*ptr;
	size_t		total_size;

	ct_putendl("-----CALLOC-----");
	if (!(total_size = count * size))
		return (NULL);
	pthread_mutex_lock(&g_malloc_lock);
	if (!g_malloc.init && malloc_init())
	{
		pthread_mutex_unlock(&g_malloc_lock);
		return (NULL);
	}
	if (!(ptr = malloc_alloc(total_size)))
		return (NULL);
	ct_bzero(ptr, total_size);
	pthread_mutex_unlock(&g_malloc_lock);
	return (ptr);
}
