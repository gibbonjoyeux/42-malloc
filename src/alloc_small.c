/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   alloc_small.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 14:33:54 by shuertas          #+#    #+#             */
/*   Updated: 2018/01/16 12:42:39 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

#define PROT	(PROT_READ | PROT_WRITE)
#define MAP		(MAP_ANON | MAP_PRIVATE)

/*
********************************************************************************
** STATIC FUNCTIONS ************************************************************
********************************************************************************
*/

static t_m_arena	*get_new_arena(void)
{
	t_m_arena		*new_arena;
	t_m_piece		*first_piece;
	size_t			i;

	if (!(new_arena = mmap(0, MALLOC_ARENA_SMALL, PROT, MAP, -1, 0)))
		return (NULL);
	i = 0;
	while (i < sizeof(t_m_arena))
		((int8_t *)new_arena)[i++] = 0;
	new_arena->length = MALLOC_ARENA_SMALL - sizeof(t_m_arena);
	first_piece = (t_m_piece*)((uint8_t*)new_arena + sizeof(t_m_arena));
	first_piece->length = MALLOC_ARENA_SMALL - sizeof(t_m_arena)
						- sizeof(t_m_piece);
	first_piece->free = 1;
	return (new_arena);
}

static void			set_piece_busy(t_m_piece *piece, size_t size)
{
	size_t			prev_size;

	if (piece->length >= size + sizeof(t_m_piece) + 1)
	{
		prev_size = piece->length;
		piece->length = size;
		piece->free = 0;
		piece->level = g_malloc.level;
		piece = (t_m_piece*)((uint8_t*)piece + sizeof(t_m_piece) + size);
		piece->length = prev_size - size - sizeof(t_m_piece);
		piece->free = 1;
	}
	else
	{
		piece->free = 0;
		piece->level = g_malloc.level;
	}
}

static void			*check_arena(t_m_arena *arena, size_t size)
{
	t_m_piece		*piece;
	size_t			i;
	uint8_t			*memory;

	memory = (uint8_t*)arena + sizeof(t_m_arena);
	i = 0;
	while (1)
	{
		piece = (t_m_piece*)(memory + i);
		if (piece->free && piece->length >= size)
		{
			set_piece_busy(piece, size);
			return (memory + i + sizeof(t_m_piece));
		}
		if ((i += piece->length + sizeof(t_m_piece)) >= arena->length)
			return (NULL);
	}
}

/*
********************************************************************************
** PUBLIC FUNCTIONS ************************************************************
********************************************************************************
*/

void				*malloc_alloc_small(size_t size)
{
	t_m_arena		*arena;
	void			*ptr;

	if (!g_malloc.small && !(g_malloc.small = get_new_arena()))
		return (NULL);
	arena = g_malloc.small;
	while (arena)
	{
		if ((ptr = check_arena(arena, size)))
			return (ptr);
		if (!arena->next)
			if (!(arena->next = get_new_arena()))
				return (NULL);
		arena = arena->next;
	}
	return (NULL);
}
