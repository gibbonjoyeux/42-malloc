/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc_level_save.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 14:33:54 by shuertas          #+#    #+#             */
/*   Updated: 2018/01/16 12:42:16 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

/*
********************************************************************************
** STATIC FUNCTIONS ************************************************************
********************************************************************************
*/

static void			save_piece(t_m_arena *arena, uint8_t *ptr)
{
	uint8_t			*memory;
	size_t			i;
	t_m_piece		*piece;

	memory = (uint8_t*)arena + sizeof(t_m_arena);
	i = 0;
	while (1)
	{
		piece = (t_m_piece*)(memory + i);
		if (ptr == (uint8_t*)piece + sizeof(t_m_piece))
		{
			if (piece->level != 0)
				piece->level--;
			return ;
		}
		i += piece->length + sizeof(t_m_piece);
		if (i >= arena->length || ptr < (uint8_t*)piece)
			break ;
	}
}

static uint8_t		check_arena(t_m_arena *arena, uint8_t *ptr)
{
	uint8_t			*memory;

	while (arena)
	{
		memory = (uint8_t*)arena + sizeof(t_m_arena);
		if (ptr >= memory && ptr < memory + arena->length)
		{
			save_piece(arena, ptr);
			return (0);
		}
		arena = arena->next;
	}
	return (1);
}

void				check_large(uint8_t *ptr)
{
	t_m_piece_l		*piece;

	piece = g_malloc.large;
	while (piece)
	{
		if ((uint8_t*)piece + sizeof(t_m_piece_l) == ptr)
		{
			if (piece->level != 0)
				piece->level--;
			return ;
		}
		piece = piece->next;
	}
}

/*
********************************************************************************
** PUBLIC FUNCTIONS ************************************************************
********************************************************************************
*/

void				alloc_level_save(void *ptr)
{
	if (!g_malloc.tiny || check_arena(g_malloc.tiny, ptr))
		if (!g_malloc.small || check_arena(g_malloc.small, ptr))
			check_large(ptr);
}
