/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   allocation.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 14:33:54 by shuertas          #+#    #+#             */
/*   Updated: 2018/01/12 15:21:00 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

#define PROT	(PROT_READ | PROT_WRITE)
#define MAP		(MAP_ANON | MAP_PRIVATE)

/*
********************************************************************************
** PUBLIC FUNCTIONS ************************************************************
********************************************************************************
*/

void				*malloc_alloc(size_t size)
{
	t_m_piece_l		*large;

	if (size < MALLOC_TINY)
		return (malloc_alloc_tiny(size));
	else if (size < MALLOC_SMALL)
		return (malloc_alloc_small(size));
	if (!(large = mmap(0, size + sizeof(t_m_piece_l), PROT, MAP, -1, 0)))
		return (NULL);
	large->length = size;
	large->level = g_malloc.level;
	large->next = g_malloc.large;
	g_malloc.large = large;
	return ((void*)((uint8_t*)large + sizeof(t_m_piece_l)));
}
