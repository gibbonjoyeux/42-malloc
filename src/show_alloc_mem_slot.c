/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_alloc_mem_slot.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 14:33:54 by shuertas          #+#    #+#             */
/*   Updated: 2018/01/16 13:20:32 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

/*
********************************************************************************
** PUBLIC FUNCTIONS ************************************************************
********************************************************************************
*/

void					show_alloc_mem_slot(void *ptr)
{
	pthread_mutex_lock(&g_malloc_lock);
	if (!g_malloc.init)
		return ;
	if (!g_malloc.tiny || malloc_show_slot_tiny_small(g_malloc.tiny, ptr))
		if (!g_malloc.small || malloc_show_slot_tiny_small(g_malloc.small, ptr))
			malloc_show_slot_large(ptr);
	pthread_mutex_unlock(&g_malloc_lock);
}
