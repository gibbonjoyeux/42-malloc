/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reallocf_tiny_small.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 14:33:54 by shuertas          #+#    #+#             */
/*   Updated: 2018/01/29 14:57:31 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

/*
********************************************************************************
** STATIC FUNCTIONS ************************************************************
********************************************************************************
*/

static void		*relocate_piece(t_m_arena *arena, t_m_piece *piece,
								t_m_piece *prev, size_t size)
{
	void		*ptr;

	if (!(ptr = malloc_alloc(size)))
	{
		free((uint8_t*)piece + sizeof(t_m_piece));
		return (NULL);
	}
	((t_m_piece*)((uint8_t*)ptr - sizeof(t_m_piece)))->level = piece->level;
	if (piece->length < size)
		ct_memcpy(ptr, (uint8_t*)piece + sizeof(t_m_piece), piece->length);
	else
		ct_memcpy(ptr, (uint8_t*)piece + sizeof(t_m_piece), size);
	malloc_free_piece(arena, piece, prev);
	return (ptr);
}

static void		*size_up_piece(t_m_arena *arena, t_m_piece *piece,
								t_m_piece *piece_prev, size_t size)
{
	t_m_piece	*piece_next;
	t_m_piece	*piece_after;

	piece_next = (t_m_piece*)((uint8_t*)piece
				+ sizeof(t_m_piece) + piece->length);
	if ((uint8_t*)piece_next
	>= (uint8_t*)arena + sizeof(t_m_arena) + arena->length
	|| !piece_next->free
	|| piece->length + piece_next->length + sizeof(t_m_piece) < size)
		return (relocate_piece(arena, piece, piece_prev, size));
	if (piece->length + piece_next->length >= size + 1)
	{
		piece_after = (t_m_piece*)((uint8_t*)piece + sizeof(t_m_piece) + size);
		piece_after->length = piece->length + piece_next->length - size;
		piece_after->free = 1;
		piece->length = size;
	}
	else
		piece->length += piece_next->length + sizeof(t_m_piece);
	return ((uint8_t*)piece + sizeof(t_m_piece));
}

static void		*size_down_piece(t_m_arena *arena, uint8_t *memory,
								t_m_piece *piece, size_t size)
{
	t_m_piece	*piece_next;
	t_m_piece	*piece_after;

	piece_next = (t_m_piece*)((uint8_t*)piece
				+ sizeof(t_m_piece) + piece->length);
	if ((uint8_t*)piece_next < memory + arena->length && piece_next->free)
	{
		piece_after = (t_m_piece*)((uint8_t*)piece + sizeof(t_m_piece) + size);
		piece_after->length = piece_next->length + (piece->length - size);
		piece_after->free = 1;
		piece->length = size;
		return ((uint8_t*)piece + sizeof(t_m_piece));
	}
	if (piece->length - size > sizeof(t_m_piece) + 1)
	{
		piece_after = (t_m_piece*)((uint8_t*)piece + sizeof(t_m_piece) + size);
		piece_after->length = piece->length - size - sizeof(t_m_piece);
		piece_after->free = 1;
		piece->length = size;
	}
	return ((uint8_t*)piece + sizeof(t_m_piece));
}

static void		*check_piece(t_m_arena *arena, uint8_t *ptr, size_t size)
{
	uint8_t		*memory;
	size_t		i;
	t_m_piece	*piece;
	t_m_piece	*piece_prev;

	piece_prev = NULL;
	memory = (uint8_t*)arena + sizeof(t_m_arena);
	i = 0;
	while (1)
	{
		piece = (t_m_piece*)(memory + i);
		if (ptr == (uint8_t*)piece + sizeof(t_m_piece))
		{
			if (piece->length == size)
				return ((uint8_t*)piece + sizeof(t_m_piece));
			if (piece->length > size)
				return (size_down_piece(arena, memory, piece, size));
			return (size_up_piece(arena, piece, piece_prev, size));
		}
		i += piece->length + sizeof(t_m_piece);
		if (i >= arena->length || ptr < (uint8_t*)piece)
			return (NULL);
		piece_prev = piece;
	}
}

/*
********************************************************************************
** PUBLIC FUNCTIONS ************************************************************
********************************************************************************
*/

uint8_t			*malloc_reallocf_tiny_small(t_m_arena *arena, uint8_t *ptr,
											size_t size)
{
	uint8_t		*memory;

	while (arena)
	{
		memory = (uint8_t*)arena + sizeof(t_m_arena);
		if (ptr >= memory && ptr < memory + arena->length)
			return (check_piece(arena, ptr, size));
		arena = arena->next;
	}
	return (NULL);
}
