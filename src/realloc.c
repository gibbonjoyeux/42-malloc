/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 14:33:54 by shuertas          #+#    #+#             */
/*   Updated: 2018/01/30 17:46:31 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

/*
********************************************************************************
** PUBLIC FUNCTIONS ************************************************************
********************************************************************************
*/

void			*realloc(void *ptr, size_t size)
{
	void		*ret;

	ct_putendl("-----REALLOC-----");
	if (!ptr)
		return (malloc(size));
	if (!size)
		size = 1;
	pthread_mutex_lock(&g_malloc_lock);
	if (!g_malloc.init)
	{
		pthread_mutex_unlock(&g_malloc_lock);
		return (NULL);
	}
	if (!g_malloc.tiny
	|| (!(ret = malloc_realloc_tiny_small(g_malloc.tiny, ptr, size))))
		if (!g_malloc.small
		|| (!(ret = malloc_realloc_tiny_small(g_malloc.small, ptr, size))))
			ret = malloc_realloc_large(ptr, size);
	pthread_mutex_unlock(&g_malloc_lock);
	return (ret);
}
