/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_large.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 14:33:54 by shuertas          #+#    #+#             */
/*   Updated: 2018/01/16 13:20:13 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

/*
********************************************************************************
** STATIC FUNCTIONS ************************************************************
********************************************************************************
*/

static void			puthex(uint8_t n)
{
	static char		*hex = "0123456789abcdef";

	ct_putchar(hex[(n / 16) % 16]);
	ct_putchar(hex[n % 16]);
	ct_putchar(' ');
}

static void			putplace(size_t n)
{
	static char		*hex = "0123456789abcdef";
	uint32_t		pow;

	pow = 16777216;
	while (pow != 0)
	{
		ct_putchar(hex[(n / pow) % 16]);
		pow /= 16;
	}
	ct_putstr("  ");
}

static void			show_piece(t_m_piece_l *piece)
{
	char			string[17];
	size_t			length;
	size_t			i;
	uint8_t			*memory;
	int8_t			j;

	length = piece->length;
	memory = (uint8_t*)piece + sizeof(t_m_piece_l);
	i = 0;
	while (i < length)
	{
		putplace(i);
		j = -1;
		while (++j < 16 && i + j < length)
		{
			puthex(memory[i + j]);
			string[j] = (ct_isprint(memory[i + j])) ? memory[i + j] : '.';
		}
		string[j] = 0;
		while (j++ < 16)
			ct_putstr("   ");
		ct_putchar(' ');
		ct_putendl(string);
		i += 16;
	}
}

/*
********************************************************************************
** PUBLIC FUNCTIONS ************************************************************
********************************************************************************
*/

void				malloc_show_slot_large(uint8_t *ptr)
{
	t_m_piece_l		*piece;

	piece = g_malloc.large;
	while (piece)
	{
		if ((uint8_t*)piece + sizeof(t_m_piece_l) == ptr)
		{
			show_piece(piece);
			return ;
		}
		piece = piece->next;
	}
}
