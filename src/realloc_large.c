/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc_large.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 14:33:54 by shuertas          #+#    #+#             */
/*   Updated: 2018/01/16 12:45:08 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

/*
********************************************************************************
** STATIC FUNCTIONS ************************************************************
********************************************************************************
*/

static void			*copy_piece(t_m_piece_l *piece, size_t size)
{
	void			*ptr;

	if (!(ptr = malloc_alloc(size)))
		return (NULL);
	if (piece->length < size)
		ct_memcpy(ptr, (uint8_t*)piece + sizeof(t_m_piece_l), piece->length);
	else
		ct_memcpy(ptr, (uint8_t*)piece + sizeof(t_m_piece_l), size);
	return (ptr);
}

/*
********************************************************************************
** PUBLIC FUNCTIONS ************************************************************
********************************************************************************
*/

uint8_t				*malloc_realloc_large(uint8_t *ptr, size_t size)
{
	t_m_piece_l		*piece;
	t_m_piece_l		*piece_prev;
	void			*ret;

	piece_prev = NULL;
	piece = g_malloc.large;
	while (piece)
	{
		if ((uint8_t*)piece + sizeof(t_m_piece_l) == ptr)
		{
			if (piece_prev)
				piece_prev->next = piece->next;
			else
				g_malloc.large = piece->next;
			ret = copy_piece(piece, size);
			munmap((void*)piece, piece->length + sizeof(t_m_piece_l));
			return (ret);
		}
		piece_prev = piece;
		piece = piece->next;
	}
	return (NULL);
}
