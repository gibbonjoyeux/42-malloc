/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 14:33:54 by shuertas          #+#    #+#             */
/*   Updated: 2018/01/30 17:52:37 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

#define PROT	(PROT_READ | PROT_WRITE)
#define MAP		(MAP_ANON | MAP_PRIVATE)

t_malloc		g_malloc = {NULL, NULL, NULL, 0, 0};
pthread_mutex_t	g_malloc_lock;

/*
********************************************************************************
** PUBLIC FUNCTIONS ************************************************************
********************************************************************************
*/

void				*malloc(size_t size)
{
	void			*ptr;

	ct_putendl("-----MALLOC-----");
	ct_putsize(size);
	ct_putchar('\n');
	if (!size)
		return (NULL);
	pthread_mutex_lock(&g_malloc_lock);
	if (!g_malloc.init && malloc_init())
	{
		pthread_mutex_unlock(&g_malloc_lock);
		return (NULL);
	}
	ptr = malloc_alloc(size);
	pthread_mutex_unlock(&g_malloc_lock);
	show_alloc_mem();
	ct_putendl("END OF MY MALLOC");
	return (ptr);
}
