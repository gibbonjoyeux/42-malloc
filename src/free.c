/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 14:33:54 by shuertas          #+#    #+#             */
/*   Updated: 2018/01/30 17:46:21 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

/*
********************************************************************************
** PUBLIC FUNCTIONS ************************************************************
********************************************************************************
*/

void					free(void *ptr)
{
	ct_putendl("-----FREE-----");
	pthread_mutex_lock(&g_malloc_lock);
	if (!g_malloc.init)
	{
		pthread_mutex_unlock(&g_malloc_lock);
		return ;
	}
	if (!g_malloc.tiny || malloc_free_tiny(ptr))
		if (!g_malloc.small || malloc_free_small(ptr))
			malloc_free_large(ptr);
	pthread_mutex_unlock(&g_malloc_lock);
}
