/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_large.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 14:33:54 by shuertas          #+#    #+#             */
/*   Updated: 2018/01/16 12:42:50 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

/*
********************************************************************************
** PUBLIC FUNCTIONS ************************************************************
********************************************************************************
*/

void				malloc_free_large(uint8_t *ptr)
{
	t_m_piece_l		*piece;
	t_m_piece_l		*piece_prev;

	piece_prev = NULL;
	piece = g_malloc.large;
	while (piece)
	{
		if ((uint8_t*)piece + sizeof(t_m_piece_l) == ptr)
		{
			if (piece_prev)
				piece_prev->next = piece->next;
			else
				g_malloc.large = piece->next;
			munmap((void*)piece, piece->length + sizeof(t_m_piece_l));
			return ;
		}
		piece_prev = piece;
		piece = piece->next;
	}
}
