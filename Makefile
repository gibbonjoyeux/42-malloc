# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: shuertas <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/11/21 14:10:28 by shuertas          #+#    #+#              #
#    Updated: 2018/02/02 10:24:45 by shuertas         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

C_GREEN 	= \033[32;01m
C_NO 		= \033[0m
C_RED 		= \033[31;01m
C_YELLOW 	= \033[33;01m
C_CYAN		= \033[36;01m
C_PINK		= \033[35;01m
C_BLACK 	= \033[30;01m

################################################################################
##[ SOURCES ]###################################################################
################################################################################

SRCS		= 	./src/malloc.c \
				./src/calloc.c \
				./src/realloc.c \
				./src/reallocf.c \
				./src/free.c \
				./src/show_alloc_mem.c \
				./src/show_alloc_mem_slot.c \
				./src/init.c \
				./src/allocation.c \
				./src/alloc_tiny.c \
				./src/alloc_small.c \
				./src/free_tiny.c \
				./src/free_small.c \
				./src/free_large.c \
				./src/free_piece.c \
				./src/realloc_tiny_small.c \
				./src/realloc_large.c \
				./src/reallocf_tiny_small.c \
				./src/reallocf_large.c \
				./src/alloc_level_up.c \
				./src/alloc_level_down.c \
				./src/alloc_level_save.c \
				./src/free_level_tiny.c \
				./src/free_level_small.c \
				./src/free_level_large.c \
				./src/show_slot_tiny_small.c \
				./src/show_slot_large.c \
				./src/main.c

OBJS		= $(SRCS:.c=.o)

ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

NAME		= libft_malloc_$(HOSTTYPE).so
LINK_NAME	= libft_malloc.so
FLAGS		= -Wall -Wextra -Werror -g
CC			= gcc
INCLUDES	= -I ./inc -I ./libct/
LIB			= libct/libct.a

BIN_NUM		= 0
BIN_MAX		= $(shell echo $(OBJS) | wc -w)

################################################################################
##[ RULES ]#####################################################################
################################################################################

##############################
##[ MAIN RULE ]###############
##############################
all: $(NAME)

##############################
##[ LIBRARY RULE ]############
##############################
$(NAME): $(OBJS)
	@tput cuu1 ; tput cuf 1
	@printf "100"
	@tput cnorm
	@rm -rf .bin_num .bin_num_tmp .bin_state
	@printf "\n$(C_GREEN)SOURCES $(C_BLACK)[$(C_YELLOW)OK$(C_BLACK)]$(C_NO)\n"
	@make -C libct
	@gcc $(FLAGS) $(OBJS) $(LIB) -shared -o $(NAME)
	@gcc $(FLAGS) $(OBJS) $(LIB) -o test
	@ln -sF $(NAME) $(LINK_NAME)
	@printf "$(C_GREEN)MALLOC $(C_BLACK)[$(C_YELLOW)DONE$(C_BLACK)]$(C_NO)\n"

##############################
##[ COMPILATION RULE ]########
##############################
%.o: %.c Makefile inc/malloc.h libct/full_libct.h libct/libct/libct.h
	@if [ -z ${INTRO} ]; then \
		tput civis; \
		printf "$(C_CYAN)[$(C_YELLOW)000 $(C_PINK)%%$(C_CYAN)]$(C_YELLOW)\n"; \
	fi
	$(eval INTRO := 1)
	@gcc $(FLAGS) $(INCLUDES) -o $@ -c $< || (tput cnorm && FAIL 2>&-)
	@$(eval BIN_NUM		:= $(shell echo "${BIN_NUM}+1" | bc))
	@$(eval BIN_STATE 	:= $(shell echo "${BIN_NUM}*100/${BIN_MAX}" | bc))
	@tput cuu1 ; tput cuf 1
	@printf "%.3d\n" ${BIN_STATE}

##############################
##[ CLEANING RULES ]##########
##############################
clean:
	@rm -rf $(OBJS)

fclean: clean
	@rm -rf $(NAME)
	@rm -rf $(LINK_NAME)

##############################
##[ RESTART RULES ]###########
##############################
re: fclean
	@make
